<?php
/**
 * Displays the page section of the theme.
 *
 * @package Theme Horse
 * @subpackage Clean_Retina
 * @since Clean Retina 1.0
 */
?>

<?php get_header(); ?>

<?php
	/** 
	 * cleanretina_before_main_container hook
	 */
	do_action( 'cleanretina_before_main_container' );
?>

<div id="container">
	<?php
		/** 
		 * cleanretina_main_container hook
		 *
		 * HOOKED_FUNCTION_NAME PRIORITY
		 *
		 * cleanretina_content 10
		 */
		do_action( 'cleanretina_main_container' );
	?>
</div><!-- #container -->

<?php
	/** 
	 * cleanretina_after_main_container hook
	 */
	do_action( 'cleanretina_after_main_container' );
?>

<?php	
	   	$options = $cleanretina_theme_options_settings;
	   	
	   	echo "<div style=\"display: none;\">";
	   		print_r($options);
	   	echo "</div>";
	   		
	   		if( 'above-slider' == $options[ 'slogan_position' ] &&  ( is_home() || is_front_page() ) ) 
   			if( function_exists( 'cleanretina_home_slogan' ) )
   				cleanretina_home_slogan(); 
   	?>
   	<?php

   		if( is_home() || is_front_page() ) {
   			if( "0" == $options[ 'disable_slider' ] ) {
   				if( function_exists( 'cleanretina_pass_cycle_parameters' ) ) 
	   				cleanretina_pass_cycle_parameters();
	   			if( function_exists( 'cleanretina_featured_post_slider' ) ) 
	   				cleanretina_featured_post_slider();
	   		}
   		}
   		else {
   			if( function_exists( 'cleanretina_breadcrumb' ) )
   				cleanretina_breadcrumb();
   		}
   	?>
 		<?php 
 			if( 'below-slider' == $options[ 'slogan_position' ] && ( is_home() || is_front_page() ) ) 
 				if( function_exists( 'cleanretina_home_slogan' ) )
 					cleanretina_home_slogan(); 
 		?>

<?php get_footer(); ?>