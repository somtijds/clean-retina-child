<?php if ( ! function_exists( 'cleanretina_featured_post_slider' ) ) :
/**
 * display featured post slider
 *
 * @uses set_transient and delete_transient
 */
function cleanretina_featured_post_slider() {	
	global $post;
		
	global $cleanretina_theme_options_settings;
   $options = $cleanretina_theme_options_settings;
	
	$cleanretina_featured_post_slider = '';
	if( ( !$cleanretina_featured_post_slider = get_transient( 'cleanretina_featured_post_slider' ) ) && !empty( $options[ 'featured_post_slider' ] ) ) {
		
		$cleanretina_featured_post_slider .= '
		<section class="featured-slider"><div class="slider-wrap"><div class="slider-cycle">';
			$get_featured_posts = new WP_Query( array(
				'posts_per_page' 			=> $options[ 'slider_quantity' ],
				'post_type'					=> array( 'post', 'page' ),
				'post__in'		 			=> $options[ 'featured_post_slider' ],
				'orderby' 		 			=> 'post__in',
				'ignore_sticky_posts' 	=> 1 						// ignore sticky posts
			));
			$i=0; while ( $get_featured_posts->have_posts()) : $get_featured_posts->the_post(); $i++;
				$title_attribute = apply_filters( 'the_title', get_the_title( $post->ID ) );
				$excerpt = get_the_excerpt();
				if ( 1 == $i ) { $classes = "slides displayblock"; } else { $classes = "slides displaynone"; }
				$cleanretina_featured_post_slider .= '
				<div class="'.$classes.'">';
						/* if( has_post_thumbnail() ) {
	
							$cleanretina_featured_post_slider .= '<figure><a href="' . get_permalink() . '" title="'.the_title('','',false).'">';
	
							$cleanretina_featured_post_slider .= get_the_post_thumbnail( $post->ID, 'slider', array( 'title' => esc_attr( $title_attribute ), 'alt' => esc_attr( $title_attribute ), 'class'	=> 'pngfix' ) ).'</a></figure>';
						} */
						if( $title_attribute != '' || $excerpt !='' ) {
						$cleanretina_featured_post_slider .= '
							<article class="featured-text">';
							if( $title_attribute !='' ) {
									$cleanretina_featured_post_slider .= the_title( '<span>','</span>', false );
								}
							if( $excerpt !='' ) {								
								$cleanretina_featured_post_slider .= $excerpt.'<a href="' . get_permalink() . '" title="'.the_title('','',false).'">'.' '.__( 'Continue Reading', 'cleanretina' ).'</a>';
							}
						$cleanretina_featured_post_slider .= '
							</article><!-- .featured-text -->';
						}
				$cleanretina_featured_post_slider .= '
				</div><!-- .slides -->';
			endwhile; wp_reset_query();
		$cleanretina_featured_post_slider .= '</div></div>				
		<nav id="controllers" class="clearfix">
		</nav><!-- #controllers --></section><!-- .featured-slider -->';
			
	set_transient( 'cleanretina_featured_post_slider', $cleanretina_featured_post_slider, 86940 );
	}
	echo $cleanretina_featured_post_slider;	
}
endif;